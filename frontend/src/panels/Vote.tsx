import {
  Button,
  Group,
  Header,
  Panel,
  PanelHeader,
  PanelHeaderBack,
  ScreenSpinner, Separator,
  SimpleCell, Spacing,
} from "@vkontakte/vkui";
import {useEffect} from "react";
// @ts-ignore
import {useFetch} from "../hooks/useFetch.ts";
// @ts-ignore
import {getVote, setAnswer} from "../services/api.ts";
// @ts-ignore
import {useVotingContext} from "../VotingContext.ts";
// @ts-ignore
import {Pages} from "../constants.ts";
import './Vote.css';

interface IProps {
  voteId: any;
  go: any;
  profileId: number;
}

export const Vote = ({go, voteId, profileId}: IProps) => {
  const isNewUi = true;
  const {profile, setPanel, callId} = useVotingContext();
  const {loading: voteLoading, data: voteData, fetchData: voteFetchData} = useFetch({method: getVote});
  const {loading: answerUploading, data: answerData, fetchData: answerFetchData} = useFetch({method: setAnswer});

  useEffect(() => {
    voteFetchData(voteId);
  }, [])

  const sendAnswer = (data) => () => {
    answerFetchData(data).then(() => {
      setPanel(Pages.VoteResult);
    })
  }

  const variants = voteData?.variants && Object.values(voteData.variants);

  return <Panel id={Pages.Vote}>
    <PanelHeader>
      Голосование
    </PanelHeader>

    {!voteLoading && voteData && (
      <Group>
        <Header>{voteData.name}</Header>
        <Spacing size={8}/>
        {!isNewUi && variants?.map((variant: any) => {
          const sendData = {
            userId: profileId, voteId: voteData.voteId, variantId: variant.variantId
          }

          return (
            <SimpleCell
              key={variant.variantId}
              after={<Button onClick={sendAnswer(sendData)}>Отдать голос</Button>}
            >
              {variant.name}
            </SimpleCell>
          )
        })}

        {isNewUi && variants?.map((variant: any) => {
          const sendData = {
            userId: profileId, voteId: voteData.voteId, variantId: variant.variantId
          }

          return (
            <div className="Vote-item" key={variant.variantId} onClick={sendAnswer(sendData)}>
              {variant.name}
            </div>
          )
        })}
      </Group>

    )}

    {voteLoading && <ScreenSpinner/>}
  </Panel>
}