const { AsyncDatabase } = require("promised-sqlite3");

const DB_FILE = `${__dirname}/db.sqlite`;

module.exports = {
  db: undefined,

  init: async () => {
    this.db = await AsyncDatabase.open(DB_FILE);

    await Promise.all([
      this.db.run(`CREATE TABLE IF NOT EXISTS votes (
        callId TEXT,
        voteId TEXT,
        name TEXT,
        createdAt INTEGER
      )`),

      this.db.run(`CREATE TABLE IF NOT EXISTS voteVariants (
        voteId TEXT,
        voteVariantId TEXT,
        name TEXT
      )`),

      this.db.run(`CREATE TABLE IF NOT EXISTS answers (
        userId TEXT,
        voteId TEXT,
        voteVariantId TEXT
      )`),
    ]);
  },

  createVote: (callId, voteId, name, variants) => {
    const promises = [];

    promises.push(
      this.db.run(`INSERT INTO votes VALUES ($callId, $voteId, $name, $now)`, {
        $callId: callId,
        $voteId: voteId,
        $name: name,
        $now: Math.floor(Date.now() / 1000),
      })
    );

    for (const variant of variants) {
      promises.push(
        this.db.run(`INSERT INTO voteVariants VALUES ($voteId, $voteVariantId, $name)`, {
          $voteId: voteId,
          $voteVariantId: variant.id,
          $name: variant.name,
        })
      );
    }

    return Promise.all(promises);
  },

  setAnswer: (userId, voteId, voteVariantId) => {
    const promises = [];

    promises.push(
      this.db.run(`DELETE FROM answers WHERE userId = $userId AND voteId = $voteId`, {
        $userId: userId,
        $voteId: voteId,
      })
    );

    if (voteVariantId) {
      promises.push(
        this.db.run(`INSERT INTO answers VALUES ($userId, $voteId, $voteVariantId)`, {
          $userId: userId,
          $voteId: voteId,
          $voteVariantId: voteVariantId,
        })
      );
    }

    return Promise.all(promises);
  },

  getVote: (voteId) => {
    return this.db.all(`
        SELECT
            votes.callId,
            votes.voteId,
            votes.name as voteName,
            voteVariants.voteVariantId as variantId,
            voteVariants.name as variantName,
            answers.userId
        FROM votes
        LEFT JOIN voteVariants ON votes.voteId = voteVariants.voteId
        LEFT JOIN answers ON answers.voteId = votes.voteId AND answers.voteVariantId = voteVariants.voteVariantId
        WHERE votes.voteId = $voteId        
    `, { $voteId: voteId });
  },

  getVoteIdsByCallId: (callId) => {
    return this.db.all(`SELECT voteId FROM votes WHERE callId = $callId ORDER BY votes.createdAt DESC`, { $callId: callId });
  },

  deleteVote: (voteId) => {
    return this.db.run(`DELETE FROM votes WHERE voteId = $voteId`, { $voteId: voteId });
  }
};
