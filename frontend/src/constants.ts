export enum Pages {
  Create = 'create',
  Vote = 'vote',
  VoteResult = 'voteResult',
  Home = 'home',
  Error = 'error',
}