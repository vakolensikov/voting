const http = require('node:http');
const querystring = require('node:querystring');
const db = require('./src/utils/db');
const controllers = require('./src/controllers');

const hostname = '127.0.0.1';
const port = 3000;

const runController = (controllerKey, req, params, callback) => {
  let body = [];

  req
    .on('data', chunk => {
      body.push(chunk);
    })
    .on('end', async () => {
      body = Buffer.concat(body).toString();
      const data = Boolean(body) && JSON.parse(body);

      const response = await controllers[controllerKey](data, params);
      callback(response);
    });
}

const server = http.createServer((req, res) => {
  const [url, queryString] = req.url.split('?');
  const controllerKey = `${req.method}:${url}`;

  // cors
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Request-Method', '*');
  res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, DELETE');
  res.setHeader('Access-Control-Allow-Headers', '*');

  if (req.method === 'OPTIONS') {
    res.writeHead(202, { 'Content-Type': 'application/json' });
    return res.end();
  }

  // 404 ошибка
  if (controllers[controllerKey] === undefined) {
    res.writeHead(404, { 'Content-Type': 'application/json' });
    return res.end();
  }

  runController(controllerKey, req, querystring.parse(queryString), (response) => {
    res.writeHead(200, { 'Content-Type': 'application/json' });
    res.end(JSON.stringify(response));
  });
});

server.listen(port, hostname, async () => {
  await db.init();
  console.log(`Server running at http://${hostname}:${port}/`);
});
