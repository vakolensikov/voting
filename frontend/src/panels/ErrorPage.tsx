import {
  Group,
  Panel,
  Spacing,
} from "@vkontakte/vkui";
// @ts-ignore
import {Pages} from "../constants.ts";


export const ErrorPage = () => {
  return <Panel id={Pages.Error}>
    <Group>
      <Spacing size={256}/>
        <h1 style={{
          textAlign: 'center',
        }}>
          В это приложение можно попасть исключительно из ВК Звонков
        </h1>
      <Spacing size={256}/>
    </Group>
  </Panel>
}