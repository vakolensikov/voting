const EventEmitter = require('events');

const _ee = new EventEmitter();
const _clients = {};

const _now = () => Math.floor(Date.now() / 1000);

const _clientKey = (callId, userId) => `${callId}_${userId}`;

const _getClient = (callId, userId) => {
  const key = _clientKey(callId, userId);

  return typeof _clients[key] !== 'undefined' && _clients[key];
}

const _createClient = (callId, userId) => {
  const key = _clientKey(callId, userId);

  const client = {
    lastTime: _now(),
    messages: [],
    removeEventHandlers: () => {}
  }

  const onAddMessage = (props) => client.messages.push(props);
  _ee.on(callId, onAddMessage);

  client.removeEventHandlers = () => _ee.off(callId, onAddMessage);

  _clients[key] = client;
}

const _registerClient = (callId, userId) => {
  const client = _getClient(callId, userId);

  if (client) {
    client.lastTime = _now();
  } else {
    _createClient(callId, userId);
  }
}

const _removeOldClients = () => {
  setTimeout(() => {
    for (const key in _clients) {
      if (_now() - _clients[key].lastTime >= 30000) {
        _clients[key].removeEventHandlers();

        delete _clients[key];
      }
    }

    _removeOldClients();
  }, 30000);
}

const callSignaling = ({ callId, cmd, data }) => {
  _ee.emit(callId, { cmd, data });

  return { success: true };
}

const readSignaling = async (_, { callId, userId }) => {
  _registerClient(callId, userId);

  const client = _getClient(callId, userId);

  if (client.messages.length) {
    const messages = [...client.messages];
    client.messages = [];

    return messages;
  }

  let _timeout = false;

  const timeoutPromise = new Promise((res) => setTimeout(() => {
    _timeout = true;
    res([]);
  }, 5000));

  const signalingPromise = new Promise((res) => {
    _ee.once(callId, ({ cmd, data }) => {
      if (!_timeout) {
        const messages = [...client.messages];
        client.messages = [];

        res(messages);
      }
    })
  })

  return await Promise.race([timeoutPromise, signalingPromise]);
}

_removeOldClients();

module.exports = {
  ['POST:/signaling']: callSignaling,
  ['GET:/signaling']: readSignaling,
}
