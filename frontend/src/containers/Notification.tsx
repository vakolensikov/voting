// @ts-ignore
import {useVotingContext} from "../VotingContext.ts";
import {Snackbar} from "@vkontakte/vkui";

export const Notification = () => {
  const {notification, setNotification} = useVotingContext()

  if(!notification) {
    return null
  };

  return <Snackbar onClose={() => setNotification(null)}>{notification.text} </Snackbar>;
}