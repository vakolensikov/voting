import {Icon28ListBulletSquareOutline, Icon24Delete} from '@vkontakte/icons'

import {
  Panel,
  PanelHeader,
  Button,
  Group,
  Div,
  IconButton,
  SimpleCell,
  ScreenSpinner,
  Spacing,
} from '@vkontakte/vkui';
// @ts-ignore
import {useFetch} from "../hooks/useFetch.ts";
// @ts-ignore
import {getVotesForCall, deleteVote} from "../services/api.ts";
// @ts-ignore
import {useVotingContext} from "../VotingContext.ts";
import {useEffect} from "react";
// @ts-ignore
import {Pages} from "../constants.ts";

interface IProps {
  id: any;
  go: any;
  goToVote: (voteId: string) => void;
}

export const Home = ({id, goToVote}: IProps) => {
  const {callId, setPanel, setNotification} = useVotingContext();

  const {loading: votesLoading, data: votes, fetchData: fetchVotes} = useFetch({method: getVotesForCall});

  useEffect(() => {
    if (!votesLoading && callId) {
      fetchVotes(callId);
    }
  }, [callId])

  const handleDelete = (voteId) => (e) => {
    e.preventDefault();
    e.stopPropagation();

    deleteVote({voteId}).then(() => {
      setNotification({text: 'Голосование успешно удалено'});
      fetchVotes(callId);
    })
  }

  return (
    <Panel id={id}>
      <PanelHeader>
        Список голосований
      </PanelHeader>
      <Group>
        <Button onClick={() => setPanel(Pages.Create)}>Создать голосование</Button>
      </Group>

      {votesLoading && <ScreenSpinner/>}
      {!votesLoading && (!votes || !votes.length) && <Group>
        <Spacing size={128}/>
        У вас нет активных голосований
        <Spacing size={128}/>
      </Group>}
      {!votesLoading && votes && !!votes.length && <Group padding="m">
        <Div>
          {
            votes.map(({name, voteId}) => {
              return (
                <SimpleCell
                  key={voteId}
                  before={<Icon28ListBulletSquareOutline/>}
                  onClick={() => {
                    goToVote(voteId);
                  }}
                  after={
                    <IconButton onClick={handleDelete(voteId)}>
                      <Icon24Delete/>
                    </IconButton>
                  }
                >
                  {name}
                </SimpleCell>)
            })
          }
        </Div>
      </Group>}

    </Panel>
  )
};
