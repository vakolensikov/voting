import {ICreateVoteParams, ICreateVoteResponse, ISetAnswerParams} from "../types";

const base = 'burning-ass.ddns.net'
const port = 443;
const apiUrl = `https://${base}:${port}`

const createRequest = <T>(url: string, method: string = 'POST') => (params: T) => {
  return fetch(url, {method, body: JSON.stringify(params)})
}
export const createVote = createRequest<ICreateVoteParams>(`${apiUrl}/vote`)

export const getVote = (id) => {
  return fetch(`${apiUrl}/vote?voteId=${id}`)
}

export const setAnswer = createRequest<ISetAnswerParams>(`${apiUrl}/answer`);

export const getVotesForCall = (id) => {
  return fetch(`${apiUrl}/vote/call?callId=${id}`)
};

export const deleteVote = createRequest<{ voteId: string }>(`${apiUrl}/vote`, 'DELETE')