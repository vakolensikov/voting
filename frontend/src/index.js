import React from "react";
import ReactDOM from "react-dom";
import bridge from "@vkontakte/vk-bridge";
import App from "./App.tsx";

// Init VK Mini App
bridge.send("VKWebAppInit").then((...values) => {
  console.log('application is running');
});

ReactDOM.render(<App />, document.getElementById("root"));