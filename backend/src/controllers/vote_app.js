const db = require('../utils/db');
const uuid = require('../utils/uuid');

const _getVoteById = async (voteId) => {
  const rows = await db.getVote(voteId);

  const vote = { callId: undefined, voteId, name: undefined, variants: {} };
  for (const row of rows) {
    // Set callId & etc.
    if (row.callId && typeof vote.callId === 'undefined') {
      vote.callId = row.callId;
      vote.name = row.voteName;
    }

    // Set variant
    if (row.variantId && typeof vote.variants[row.variantId] === 'undefined') {
      vote.variants[row.variantId] = {
        variantId: row.variantId,
        name: row.variantName,
        answers: []
      }
    }

    // Set answers
    if (row.userId) {
      vote.variants[row.variantId].answers.push(row.userId);
    }
  }

  return vote;
}

const getVoteById = (_, { voteId }) => {
  return _getVoteById(voteId);
}

const getVotesByCallId = async (_, { callId }) => {
  const rows = await db.getVoteIdsByCallId(callId);
  const votes = [];
  for (const row of rows) {
    const voteId = row['voteId'];
    const vote = await _getVoteById(voteId);
    votes.push(vote);
  }

  return votes;
}

const createVote = async ({ callId, name, variants }) => {
  const voteId = uuid();
  await db.createVote(
    callId,
    voteId,
    name,
    variants.map((name) => ({
      id: uuid(),
      name
    })
  ));

  return _getVoteById(voteId);
}

const setAnswer = async ({ userId, voteId, variantId }) => {
  await db.setAnswer(userId, voteId, variantId);

  return _getVoteById(voteId);
}

const deleteAnswer = async ({ userId, voteId }) => {
  await db.setAnswer(userId, voteId);

  return _getVoteById(voteId);
}

const deleteVote = async ({ voteId }) => {
  await  db.deleteVote(voteId);

  return {};
}

module.exports = {
  ['POST:/vote']: createVote,
  ['GET:/vote']: getVoteById,
  ['GET:/vote/call']: getVotesByCallId,
  ['POST:/answer']: setAnswer,
  ['DELETE:/vote']: deleteVote,
  ['DELETE:/answer']: deleteAnswer,
}
