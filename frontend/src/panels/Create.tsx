import {useState} from 'react';

import {
  Panel,
  PanelHeader,
  FormItem,
  Group,
  FormLayout,
  Input,
  FormLayoutGroup,
  Button,
  ButtonGroup, IconButton, Spacing, Separator
} from '@vkontakte/vkui';
import {Icon12Add, Icon12Delete, Icon24Add} from '@vkontakte/icons';
// @ts-ignore
import {createVote} from "../services/api.ts";
// @ts-ignore
import {useVotingContext} from "../VotingContext.ts";
import bridge from '@vkontakte/vk-bridge';
// @ts-ignore
import {Pages} from "../constants.ts";


const Create = () => {
  const {setNotification, callId, setPanel, setCurrentVoteId} = useVotingContext();
  const [title, setTitle] = useState('');
  const [answers, setAnswers] = useState({[Date.now()]: ''});

  const addAnswer = () => {
    setAnswers(state => {
      return {...state, [Date.now()]: ''}
    })
  }

  const changeAnswer = (id, value) => {
    setAnswers(state => {
      return {
        ...state,
        [id]: value,
      }
    })
  }

  const removeAnswer = (id) => () => {
    setAnswers(state => {
      delete state[id];

      return {...state};
    })
  }

  const submit = () => {
    createVote({
      name: title,
      variants: Object.values(answers),
      callId,
    }).then((res) => {
      res.json().then((vote) => {
        setNotification({text: 'Голосование создано'});
        setPanel(Pages.Vote);
        setCurrentVoteId(vote.voteId);

        // @ts-ignore
        bridge.send('VKWebAppCallApp', {appId: 51767771, voteId: vote.voteId})
          .then((data) => {
            console.log(data);
          })
          .catch((error) => {
            // Ошибка
            console.log(error);
          });
      })
    })
  }

  const handleChangeAnswer = (id) => e => {
    changeAnswer(id, e.target.value)
  }

  return (
    <Panel id={Pages.Create}>
      <PanelHeader>
        Созданиие голосования
      </PanelHeader>
      <Group>
        <Spacing size={14}/>
        <FormLayout>
          <FormItem bottom="Введите заголовок голосования">
            <Input
              placeholder="Введите заголовок голосования"
              onChange={e => setTitle(e.target.value)}
            />
          </FormItem>
          <Spacing size={18}/>

          {Object.entries(answers).map(([id, value], index) => {
            return (
              <FormItem bottom={`Ответ ${index + 1}`} key={id}>
                <Input
                  placeholder={`Введите ответ ${index + 1}`}
                  value={value}
                  onChange={handleChangeAnswer(id)}
                  after={<Icon12Delete onClick={removeAnswer(id)}/>}
                />
              </FormItem>
            )
          })}

          <Group mode="plain" style={{
            paddingLeft: '12px'
          }}>
            <Spacing size={28}/>
            <div
              style={{
                display: 'flex',
                justifyContent: 'flex-end',
                paddingRight: '12px',
              }}
            >
              <ButtonGroup>
                <Button onClick={addAnswer} after={<Icon12Add/>} mode="secondary">Добавить вариант</Button>
                <Button onClick={submit}>
                  Создать голосование
                </Button>
              </ButtonGroup>
            </div>
          </Group>
        </FormLayout>
      </Group>

    </Panel>
  )
};

export default Create;
