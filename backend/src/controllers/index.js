const voteAppControllers = require('./vote_app');
const longpollControllers = require('./longpoll');

module.exports = {
  ...voteAppControllers,
  ...longpollControllers,
}
