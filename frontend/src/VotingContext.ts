import {createContext, useContext} from "react";

const init = {
  callId: '',
  setCallId: (id) => {},
  setPanel: (id: any) => {},
  profile: null,
  setProfile: (profile: any) => {},
  notification: null,
  setNotification: (data: any) => {},
  currentVoteId: '',
  setCurrentVoteId: (id: any) => {},
}

export const VotingContext = createContext(init);

export const useVotingContext = () => {
  const context = useContext(VotingContext);

  return context;
}