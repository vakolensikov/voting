import {useState} from "react";

interface IParams<RequestParams> {
  method: (data: RequestParams) => Promise<Response>;
}

export const useFetch = <ReqParams, ReqResponse>({method}: IParams<ReqParams>) => {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState<ReqResponse | null>(null)

  const fetchData = (params: ReqParams) => {
    setLoading(true);
    return method(params).then(res => {
      res.json().then((data) => {
        setData(data as ReqResponse)
      });
    }).finally(() => {
      setLoading(false);
    })
  }

  return {fetchData, data, loading}
}