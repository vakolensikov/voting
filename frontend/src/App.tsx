import {useState, useEffect} from "react";
import bridge from '@vkontakte/vk-bridge';
import {View, AdaptivityProvider, AppRoot, ConfigProvider, SplitLayout, SplitCol} from '@vkontakte/vkui';
import '@vkontakte/vkui/dist/vkui.css';

// @ts-ignore
import {Home} from './panels/Home.tsx';
// @ts-ignore
import Create from './panels/Create.tsx';
// @ts-ignore
import {Vote} from "./panels/Vote.tsx";
// @ts-ignore
import {VoteResult} from "./panels/VoteResult.tsx";
// @ts-ignore
import {VotingContext} from "./VotingContext.ts";
// @ts-ignore
import {Notification} from "./containers/Notification.tsx";
// @ts-ignore
import {Pages} from "./constants.ts";
// @ts-ignore
import {ErrorPage} from "./panels/ErrorPage.tsx";

const getInitialPage = (callId, voteId) => {
  if(callId) {
    return Pages.Create
  }

  if(voteId) {
    return Pages.Vote
  }

  return Pages.Error
}

const App = () => {
  const initCallId = new URLSearchParams(location.hash.replace('#', '')).get('callId') || '';
  const initVoteId = new URLSearchParams(location.hash.replace('#', '')).get('voteId') || '';

  const [callId, setCallId] = useState(initCallId);
  const [currentVoteId, setCurrentVoteId] = useState(initVoteId);
  const [notification, setNotification] = useState(null);
  const [activePanel, setActivePanel] = useState(getInitialPage(callId, currentVoteId));
  const [profile, setProfile] = useState(null);

  useEffect(() => {
    async function fetchData() {
      const user = await bridge.send('VKWebAppGetUserInfo');
      setProfile(user);
    }

    fetchData();

  }, []);


  const go = e => {
    setActivePanel(e.currentTarget.dataset.to);
  };

  const goToVote = (voteId) => {
    setActivePanel('vote');
    setCurrentVoteId(voteId);
  }

  return (
    <ConfigProvider>
      <AdaptivityProvider>
        <AppRoot>
          <SplitLayout>
            <SplitCol>
              <VotingContext.Provider value={{setPanel: setActivePanel, setProfile, profile, notification, setNotification, callId, setCallId, currentVoteId, setCurrentVoteId}}>
                <View activePanel={activePanel}>
                  <Home id={Pages.Home} go={go} goToVote={goToVote}/>
                  <Create id={Pages.Create} go={go}/>
                  <Vote id={Pages.Vote} go={go} voteId={currentVoteId} profileId={profile?.id}/>
                  <VoteResult id={Pages.VoteResult} go={go} voteId={currentVoteId}/>
                  <ErrorPage id={Pages.Error}/>
                </View>
                <Notification/>
              </VotingContext.Provider>
            </SplitCol>
          </SplitLayout>
        </AppRoot>
      </AdaptivityProvider>
    </ConfigProvider>
  );
}

export default App;
