import {
  Group,
  Header,
  Panel,
  PanelHeader,
  PanelHeaderBack,
  ScreenSpinner, Separator,
  SimpleCell, Spacing,
} from "@vkontakte/vkui";
import {useEffect, useRef, useState} from "react";
// @ts-ignore
import {useFetch} from "../hooks/useFetch.ts";
// @ts-ignore
import {getVote, setAnswer} from "../services/api.ts";
// @ts-ignore
import {useVotingContext} from "../VotingContext.ts";
// @ts-ignore
import {Pages} from "../constants.ts";
import { Icon12Check } from '@vkontakte/icons';
import './VoteResult.css';

interface IProps {
  voteId: any;
}

export const VoteResult = ({voteId}: IProps) => {
  const isNewUi = true;
  const {profile} = useVotingContext();
  const [firstRender, setFirstRender] = useState(true);
  const interval = useRef(null);
  const {loading: voteLoading, data: voteData, fetchData: voteFetchData} = useFetch({method: getVote});

  useEffect(() => {
    voteFetchData(voteId).then(() => {
      setFirstRender(false);
    });

  }, [])

  useEffect(() => {
    if (interval.current) {
      clearInterval(interval.current);
      interval.current = null;
    }
    interval.current = setInterval(() => voteFetchData(voteId), 3000);

    return () => {
      if (interval.current) {
        clearInterval(interval.current);
      }
    }
  }, []);

  const total: number = (voteData && voteData.variants ? Object.values(voteData.variants).reduce((acc, item: any) => {
    const count = item?.answers?.length

    return acc + (count || 0);
  }, 0) : 0) as number;

  return <Panel id={Pages.VoteResult}>
    <PanelHeader>
      Результат голосования
    </PanelHeader>

    {voteData && (
      <Group>
        <Header>{voteData.name}</Header>
        <Spacing size={8}/>
        {!isNewUi && Object.values(voteData.variants).map((variant: any) => {
          return <SimpleCell key={variant.variantId} after={variant.answers.length}>
            {variant.name}
          </SimpleCell>
        })}

        {isNewUi && Object.values(voteData.variants).map((variant: any) => {
          const isMe = variant.answers?.includes(String(profile.id));
          const count = variant.answers?.length || 0;
          const percent = Math.round(count / total * 100)

          return (
            <div className="VoteResult-item" key={variant.variantId}>
              {!!percent && <div className="VoteResult-progress" style={{width: `${percent}%`}}/>}
              <div className="VoteResult-item-inner">
                <span>{variant.name}
                  {!!count && <span style={{color: '#626d7a'}}> · {count}</span>}
                </span>
                {!!percent && <span className="Vote-item-percent">
                  {isMe && <Icon12Check style={{
                    color: '#447bba',
                    marginRight: '5px',
                  }}/>}
                  {percent}%
                </span>}
              </div>
            </div>
          )
        })}
      </Group>

    )}
    {voteLoading && firstRender && <ScreenSpinner/>}
  </Panel>
}