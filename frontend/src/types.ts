import {string} from "prop-types";

export interface ICreateVoteParams {
  callId: string,
  name: string,
  variants: string[]
}

export interface ICreateVoteResponse {

}

export interface ISetAnswerParams {
  userId: number,
  voteId: string,
  variantId: string,
}